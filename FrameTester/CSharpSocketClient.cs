﻿using System;
using System.Net;
using System.Net.Sockets;
 
// This C# "Console Application" example program demonstrates SCPI
// communication with an Keysight TCP socket-enabled instrument that
// supports socket "control connections" (such as PNA network analyzers,
// which have support for control connections in their socket
// implementation as of PNA Firmware A.08.33.01).
namespace CSharpSocketClient
{
  /// <summary>
  /// The class supporting the main entry point for the application.
  /// </summary>
  class MainClass
  {
   static AsyncCallback m_pCallbackFunc;
   static string m_AsyncReply;
 
   /// <summary>
   /// The main entry point for the application.
   /// </summary>
   //[STAThread]
   //static int Main(string[] args)
   //{
   //  try
   //  {
   //   if (args.Length != 1)
   //   {
   //     Console.WriteLine("");
   //     Console.WriteLine("Usage -- with Microsoft's .NET runtime:");
   //     Console.WriteLine("SocketClient servernameoraddress");
   //     Console.WriteLine("Example: SocketClient 192.168.0.1");
   //     Console.WriteLine("");
   //     Console.WriteLine("Usage -- with Mono's (www.mono-project.com) .NET runtime:");
   //     Console.WriteLine("mono SocketClient.exe servernameoraddress");
   //     Console.WriteLine("Example: mono SocketClient.exe 192.168.0.1");
   //     return 1;
   //   }
 
   //   string server = args[0];
   //   Int32 port = 5025; // default socket port number for the PNA
 
   //   // Create the primary client socket instance
   //   Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
 
   //   // Get the DNS IP addresses associated with the instrument.
   //   // (if 'server' string contains the IP address rather than DNS name, this still works)
   //   IPHostEntry hostInfo = Dns.Resolve(server);
   //   IPAddress[] IPaddresses = hostInfo.AddressList;
   //   if (IPaddresses.GetLength(0) < 1)
   //     return 1;
 
   //   // Create an endpoint to use for opening the socket connection
   //   IPEndPoint endpoint1 = new IPEndPoint (IPaddresses[0], port);
   //   // Open the connection to the server instrument
   //   client.Connect(endpoint1);
   //   if(!client.Connected)
   //     return 1;
 
   //   // Query the instrument's ID string.
   //   string id = Parse(client, "*IDN?");
 
   //   // Clear the instrument's Status Byte
   //   Parse(client, "*CLS");
 
   //   // Enable for the OPC bit (bit 0, which has weight 1) in the instrument's
   //   // Event Status Register, so that when that bit's value transitions from 0 to 1
   //   // then the Event Status Register bit in the Status Byte (bit 5 of that byte)
   //   // will become set.
   //   Parse(client, "*ESE 1");
 
   //   // Enable for bit 5 (which has weight 32) in the Status Byte to generate an
   //   // SRQ when that bit's value transitions from 0 to 1.
   //   Parse(client, "*SRE 32");
 
   //   // Ask the instrument for the number of a port on which a 'control'
   //   // socket connection can be opened.
   //   string controlPortNumStr = Parse(client, "SYSTem:COMMunicate:TCPip:CONTrol?");
   //   Int32 controlPortNum = System.Convert.ToInt32(controlPortNumStr);
 
   //   // Create the client "control connection" socket instance
   //   Socket controlClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      
   //   // Create an endpoint to use for opening the control connection
   //   IPEndPoint endpoint2 = new IPEndPoint (IPaddresses[0], controlPortNum);
   //   // Connect to the server instrument via the port number that was returned by the instrument.
   //   controlClient.Connect(endpoint2);
   //   if(!controlClient.Connected)
   //     return 1;
        
   //   // Start the control connection listening for an SRQ message.
   //   BeginListeningForAsyncReply(controlClient);
 
   //   // Now send a preset command to the instrument, accompanied by '*OPC' such
   //   // that when that operation is complete an SRQ event will be generated
   //   // which posts the Status Byte message on the control connection.
   //   Parse(client, "SYSTem:PRESet;*OPC");
 
   //   // Normally at this point you would want to have your program do other things
   //   // right here until the SRQ callback occurs, instead of just idling here waiting
   //   // for it.
   //   do {} while (m_AsyncReply == null);
 
   //   // Now that the SRQ has occurred, we can issue a Device Clear via the control connection.
   //   Parse(controlClient, "DCL");
 
   //   // The instrument will respond back with "DCL" (and linefeed character appended
   //   // on the end) via the control connection when it has finished processing the
   //   // Device Clear request. Note that this 'Response' method uses the synchronous
   //   // form of 'Receive', so it could potentially time out if the instrument were
   //   // to take a long time to process the Device Clear. So alternatively the
   //   // 'BeginListeningForAsyncReply' could be used for this instead of 'Response'.
   //   string deviceClearResponse = Response(controlClient);
 
   //   // Close both of the socket client sessions.
   //   controlClient.Close();
   //   client.Close();
   //  }
   //  catch (ArgumentNullException e)
   //  {
   //   Console.WriteLine("ArgumentNullException: {0}", e);
   //  }
   //  catch (SocketException e)
   //  {
   //   Console.WriteLine("SocketException: {0}", e);
   //  }
 
   //  Console.WriteLine("/n Press Enter to continue...");
   //  Console.Read();
   //  return 0;
   //}
  
   static string Parse(Socket client, string command)
   {
     // Translate the passed command into ASCII and store it as a Byte array.
     Byte[] data = System.Text.Encoding.ASCII.GetBytes(command);   
     // Send the command to the socket-enabled instrument.
     client.Send(data);
     // Has to be followed by a linefeed character as terminator.
     string s = "/n";
     Byte[] lf = new Byte[1];
     lf[0] = Convert.ToByte(s);//{(Byte)'/n'};
     client.Send(lf);
     Console.WriteLine("Sent: {0}", command);   
     // If the message was a query (involved a question mark), receive the instrument response.
     if (command.IndexOf("?") >= 0)
     {
      return Response(client);
     }
     return "";
   }
 
   static string Response(Socket client)
   {
     // Buffer to store the response bytes.
     // For simplicity of this example, we allocate just for a 256-byte maximum
     // response size.
     Byte[] data = new Byte[256];
     // Read the batch of response bytes.
     Int32 byteCount = client.Receive(data);
     // String to store the response ASCII representation.
     string responseData = System.Text.Encoding.ASCII.GetString(data, 0, byteCount);
     Console.WriteLine("Received: {0}", responseData);
     return responseData;
   }
 
   static void BeginListeningForAsyncReply(Socket client)
   {
     if (m_pCallbackFunc == null)
     {
      m_pCallbackFunc = new AsyncCallback(OnMessageReceived);
     }
     SocketPacket socPkt = new SocketPacket();
     socPkt.thisSocket = client;
     // Start asynchronously listening for a response from this client
     IAsyncResult result = client.BeginReceive (socPkt.data,
      0, socPkt.data.Length,
      SocketFlags.None,
      m_pCallbackFunc,
      socPkt);
   }
 
   class SocketPacket
   {
     public Socket thisSocket;
     // For simplicity of this example, we allocate just for a 256-byte maximum
     // response size.
     public Byte[] data = new Byte[256];
   }
 
   static void OnMessageReceived(IAsyncResult asyn)
   {
     SocketPacket socPkt = (SocketPacket)asyn.AsyncState ;
     Int32 byteCount = socPkt.thisSocket.EndReceive(asyn);
     m_AsyncReply = System.Text.Encoding.ASCII.GetString(socPkt.data, 0, byteCount);
     Console.WriteLine("Received: {0}", m_AsyncReply);
   }  
  }
}
