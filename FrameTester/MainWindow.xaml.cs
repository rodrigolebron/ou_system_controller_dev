﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OU_System_Controller;
using System.Threading;
using MicroEpsilon;
//using CSharpSocketClient;

namespace FrameTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        
        //MainClass client;
        SystemController sc;
        string FileNameCameraMsg =""; 
        string FileName = "Learn_positions";
        string FileNameJoint = "CoordinatesFirstColCenteredV2";
        string FileNameJointCentered = "CoordinatesCenter3by3";
        int nbr_of_msrmnts_per_point = 1
            ;

        static float delta = (float) 0.016; //distance btwn elements Xband
        //float delta = 0.05058/2; //distance btwn elements Horus

        DistanceSensor ds;

        public MainWindow()
        {
            InitializeComponent();
            sc = new SystemController();
            ds = new DistanceSensor();
            //client = new MainClass();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            sc.getCameraData();
        }

        private void Down_Click(object sender, RoutedEventArgs e)
        {
            sc.moveFromCurrentPosition(0, -delta, 0);
            //sc.moveFromCurrentPosition(-7.75 * .0254, 7.75 * .0254, 0);
        }

        private void Up_Btn_Click(object sender, RoutedEventArgs e)
        {
            //Console.WriteLine("UP");
            sc.moveFromCurrentPosition(0, delta, 0);
        }

        private void LeftBtn_Click(object sender, RoutedEventArgs e)
        {
            sc.moveFromCurrentPosition(-delta, 0, 0);
        }

        private void RightBtn_Click(object sender, RoutedEventArgs e)
        {
            sc.moveFromCurrentPosition(delta , 0, 0);
        }

        private void GoToTop_Click(object sender, RoutedEventArgs e)
        {
            //sc.goToOriginJointDeg(-37.78, -95.33, 67.73, -150.70, -67.37, -91.57, 4); // top left corner - left center -90 rotZ
            //sc.goToOriginJointDeg(-39.03, -95.33, 66.72, -149.61, -66.00, -1.83, 4); // top left corner - top center
            //sc.goToOriginJointDeg(-39.36,-97.62,74.23,-154.85,-65.73,-1.86,4); //top part of same element as in GoToBottom_Click()
            //sc.goToOriginJointDeg(-58.93, -97.13, 95.04, -175.56, -46.05, -2.71, 4); // top left corner - top center
            sc.goToOriginJointDeg(-16.94,-92.32,70.58,-156.74,-88.07,-1.17, 4); // top left point
            Console.WriteLine("Top distance" + sc.getCameraTopDistance().ToString());
        }

        private void GoToBottom_Click(object sender, RoutedEventArgs e)
        {
            sc.goToOriginJointDeg(-39.36, -98.17, 76.20, -156.27, -65.68, -1.86, 4);
            Console.WriteLine("Bottom distance" +  sc.getCameraBottomDistance().ToString());
            
        }

        private void GoToInBtwn_Click(object sender, RoutedEventArgs e)
        {
            sc.goToOriginJointDeg(-39.40, -98.88, 78.88, -158.20, -65.61, -1.87, 4);
            Console.WriteLine("In Between distance" + sc.getCameraInBtwnDistance().ToString());
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //sc.saveCoordidnates(FileName, true);
        }

        private void Save_Coordinates_Click(object sender, RoutedEventArgs e)
        {
            getAndStoreCameraMsg(FileNameCameraMsg);
        }

        private void getAndStoreCameraMsg(string filename)
        {
            //Read camera output string
            string msg = sc.getCameraString();
            
            //store data with error handling
            for (int i = 0; i <= 10; i++)
            {

                try
                {
                    System.IO.File.AppendAllText(filename, msg + "\n");
                }
                catch
                {
                    Console.WriteLine("Error while trying to save into the data file");
                }
            }
        }

        private void Load_Coord_Click(object sender, RoutedEventArgs e)
        {
            sc.readFromFileJointPosition(FileNameJoint);
            sc.moveToJointPosition(0, 4);
        }

        private void Scan_Click(object sender, RoutedEventArgs e)
        {
            FileNameCameraMsg = "..\\..\\..\\..\\..\\..\\" + DateTime.Now.ToString("dd-MM_hh-mm") + "CameraData-40deg.csv";
            int timeDelayMs = 5000;
            //INSPECT COLUMNS
            //loop for x pos
            for (int t = 0; t <= 5; t++)
            {
                for (int i = 0; i <= (4 - 1); i++)
                {
                    //loop for y pos
                    for (int j = 0; j <= (2 - 1); j++)
                    {
                        //show current pos
                        Console.WriteLine("Vertical: Col=" + i.ToString() + " Row=" + j.ToString());
                        //move to initial position
                        sc.goToOriginJointDeg(-16.95, -92.81, 72.69, -158.33, -88.07, -1.21, 4); //top left corner - top left
                        //move relative
                        sc.moveFromCurrentPosition(delta * i, -delta * j, 0);
                        //wait to vibration
                        Thread.Sleep(timeDelayMs);
                        //Measurement loop
                        for (int k = 1; k <= nbr_of_msrmnts_per_point; k++)
                        {
                            //get and store a lot of measurements

                            //Read camera output string
                            string msg = sc.getCameraString();

                            //store data with error handling
                            for (int m = 0; m <= 10; m++)
                            {

                                try
                                {
                                    System.IO.File.AppendAllText(FileNameCameraMsg, j.ToString() + "," + i.ToString() + "," + msg + "\n");
                                    break;
                                }
                                catch
                                {
                                    Console.WriteLine("Error while trying to save into the data file");
                                }
                            }
                        }

                        Console.WriteLine(Environment.NewLine + "ITERATION: " + i.ToString());
                    }
                }
            }
            
            //INSPECT ROWS
            //loop for x pos
            for (int i = 1; i <= (3 - 1); i++)
            {
                //loop for y pos
                for (int j = -1; j <= (3 - 1); j++)
                {
                    //show current pos
                    Console.WriteLine("Horizontal: Col=" + i.ToString()+" Row="+j.ToString());
                    
                    //move to initial position
                    sc.goToOriginJointDeg(-16.95, -92.81, 72.69, -158.33, -88.07, -1.21, 4); //top left corner - top left
                    //move relative
                    sc.moveFromCurrentPosition(delta * i, -delta * j, 0);
                    //Horizontal position
                    sc.moveToHpos();
                    //wait to vibration
                    Thread.Sleep(timeDelayMs);
                    //Measurement loop
                    for (int k = 1; k <= nbr_of_msrmnts_per_point; k++)
                    {
                        //get and store a lot of measurements
                        //Read camera output string
                        string msg = sc.getCameraString();

                        //store data with error handling
                        for (int m = 0; m <= 10; m++)
                        {

                            try
                            {
                                System.IO.File.AppendAllText(FileNameCameraMsg, j.ToString() + "," + i.ToString() + "," + msg + "\n");
                                break;
                            }
                            catch
                            {
                                Console.WriteLine("Error while trying to save into the data file");
                            }
                        }
                    }


                    Console.WriteLine(Environment.NewLine + "ITERATION: " + i.ToString());
                }
            }
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmss");
        }

        private void BtnDistance_Click(object sender, RoutedEventArgs e)
        {
            //ds.getMeasurement();
            sc.getDistanceFromSensor();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //int index = 0;
            //double measuredDistance = 0;
            ////Move to first element
            ////sc.goToOriginJointDeg(-38.23, -95.35, 66.70, -149.58, -66.80, -1.81, 4); //top left corner - top left
            ////sc.goToOriginJointDeg(-39.03, -95.33, 66.72, -149.61, -66.00, -1.83, 4); // top left corner - top center
            ////sc.goToOriginJointDeg(-37.78, -95.33, 67.73, -150.70, -67.37, -91.57, 4); // top left corner - left center -90 rotZ
            //sc.saveJointPosition(FileNameJointCentered);
            //while (index != 3)
            //{
            //    sc.centerFiducial(120e-6);
            //    //sc.moveFromCurrentPosition(0.05058 / 3, 0, 0); // move right
            //    sc.moveFromCurrentPosition(0, -0.05058, 0); // move down
            //    sc.saveJointPosition(FileNameJointCentered);
            //    index++;
            //    Thread.Sleep(2000);
                sc.centerFiducial(120e-6);
            //s}
            
           
        }

        private void txtTest_Click(object sender, RoutedEventArgs e)
        {
           // sc.getMFCregistry2(50, 1, true).ToString();
            
            ////test to store a pos, move, and go back to the stored pos
            //sc.saveJointPosition("coordinates.txt"); //save current pos
            //sc.goToOriginJointDeg(-32.28, -103.40, 73.53, -148.04, -60.07, -1.81, 4); // Flir best snapping position 
            //sc.readFromFileJointPosition("coordinates.txt"); //read
            //sc.moveToJointPosition(sc.numberOfReadCoordinates - 1,4);

            //move to the position of the first element for NF to FF meas, from the top left element. Xband Rf Core antenna
            sc.moveFromCurrentPosition(-0.146, 0.146, 0);

        }

        private void btnConnect2Pna_Click(object sender, RoutedEventArgs e)
        {
            
        }

       

        

       
    }
}
