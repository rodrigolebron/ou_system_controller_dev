﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Threading.Tasks;
//using Jai_FactoryDotNET;
//using System.Runtime.InteropServices;
//using System.Threading;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.IO;
//using System.Windows;


//namespace OU_System_Controller
//{
//    public class FlirInterface
//    {
//        CFactory _factory = new CFactory();

//        CCamera _camera;



//        public string fidNumber;
//        string directory;
//        public string fileName;
//        private static bool firstFrame;
//        SystemController systemController;

//        public FlirInterface(SystemController sc)
//        {
//            systemController = sc;
//            _factory.Open();
//            directory = "";

//        }

//        public void Connect()
//        {
//            try
//            {
//                _factory.UpdateCameraList(CFactory.EDriverType.FilterDriver);

//                if (_factory.CameraList.Count > 0)
//                {
//                    for (int i = 0; i < _factory.CameraList.Count; i++)
//                    {
//                        if (_factory.CameraList[i].IPAddress == "192.168.0.44")
//                        {
//                            _camera = _factory.CameraList[i];
//                        }
//                    }
//                    if (_camera != null)
//                    {
//                        _camera.Open();
//                        _camera.NewImageDelegate += _camera_NewImageDelegate;

//                    }
//                }
//            }
//            catch (Exception E)
//            {
//                Log.WriteLine(E.Message + "\n" + E.StackTrace);
//            }
//        }

//        public void Disconnect()
//        {
//            try
//            {
//                if (_camera != null)
//                {
//                    _camera.StopAcquisition();
//                    _camera.Close();
//                }
//            }
//            catch (Exception E)
//            {
//                Log.WriteLine(E.Message + "\n" + E.StackTrace);
//            }
//        }

//        public void Start()
//        {
//            try
//            {
//                _camera.GetNode("AutoFocus").ExecuteCommand();
//                _camera.GetNode("AcquisitionMode").Value = "Continuous";

//                _camera.StartImageAcquisition(false, 1);
//            }
//            catch (Exception E)
//            {
//                Log.WriteLine(E.Message + "\n" + E.StackTrace);
//            }
//        }


//        private void _camera_NewImageDelegate(ref Jai_FactoryWrapper.ImageInfo ImageInfo)
//        {
//            ImageInfo.ImageBuffer;
//            if (SystemController.wantFrame)
//            {
//                SystemController.wantFrame = false;
//                byte[] buf = new byte[ImageInfo.ImageSize];
//                Marshal.Copy(ImageInfo.ImageBuffer, buf, 0, (int)ImageInfo.ImageSize);
//                SaveRawImage(fileName, buf, 640, 480, PixelFormats.Gray16);
//            }

//        }


//        public void updateFileName(string file)
//        {
//            fileName = file;
//        }
//        internal void Save(string file)
//        {
//        }
//        internal void SaveRawImage(String path, byte[] data, int width, int height, PixelFormat pf)
//        {
//            try
//            {
//                WriteableBitmap wBmp = new WriteableBitmap(width, height, 96, 96, pf, null);
//                int stride = width;
//                if (pf == PixelFormats.Gray16)
//                {
//                    stride = width * 2;
//                }

//                wBmp.WritePixels(new Int32Rect(0, 0, width, height), data, stride, 0);

//                TiffBitmapEncoder tiff = new TiffBitmapEncoder();
//                tiff.Frames.Add(BitmapFrame.Create(wBmp));

//                using (Stream stm = File.Create(path + ".tiff"))
//                {
//                    tiff.Save(stm);
//                }


//            }

//            catch (Exception E)
//            {
//                Log.WriteLine(E.Message + "\n" + E.StackTrace);
//            }
//        }
//        public void Stop()
//        {
//            _camera.StopImageAcquisition();
//        }

//    }
//}
