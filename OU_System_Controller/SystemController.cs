﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using MicroEpsilon;


namespace OU_System_Controller
{
    public class SystemController
    {
        private RobotInterface robot;
        private CameraInterface cameraInterface;
        private CameraInfo cameraInfo;
        private DistanceSensor distanceSensor;
        private MFCControlBit mfcControlBit;
     //   private FlirInterface flirInterface;
        private double[] fiducialCoords;

        private double xOffset, yOffset, thetaOffset, laserXOffset;
        private double pixelToMeterConstant = 0.00002351074218;
        private static string[] jointPositions;
        public string baseExpirementDirectory;
        public int numberOfReadCoordinates;

        //private List<string> toolHeadCoordinates;
        public static bool wantFrame;

        public string resultFileName;
        //private bool Running;

        //string expirementDirectory = @"C:\Code\UR_RadarAnalyzer\Expirement1\", string resultsFile = @"Results.csv"
        public SystemController()
        {
            try
            {
                robot = new RobotInterface();
                cameraInterface = new CameraInterface();
                cameraInfo = new CameraInfo();
                distanceSensor = new DistanceSensor();
                mfcControlBit = new MFCControlBit();
       //         flirInterface = new FlirInterface(this);
                wantFrame = false;
                fiducialCoords = new double[4];
                double[] robotOrgin = new double[6];
         //       flirInterface.Connect();

            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }
        }

        // calculates all neccesary offsets from camera results and
        // stores them in variables
        private bool calculateFiducialOffsetFromCenter()
        {
            bool tmp;
            double thetaLimit = 20;
            tmp = getCameraMeasurementForCenter();
            fiducialCoords = cameraInfo.getFiducialCoords();

            xOffset = fiducialCoords[0] - 1024.0;
            yOffset = fiducialCoords[1] - 554.0;
            thetaOffset = fiducialCoords[2] - 90.0;
            if (Math.Abs(thetaOffset) > thetaLimit) // If the error is too big then is probably a bad recognition
            {
                thetaOffset = -thetaOffset / Math.Abs(thetaOffset);//20 * thetaOffset / Math.Abs(thetaOffset);
            }

            laserXOffset = 0;
            return tmp;
        }

        // converts pixel offsets to meters because the
        // robot takes values in meters
        private void convertPixelsToMeters()
        {
            xOffset = xOffset * pixelToMeterConstant;
            yOffset = yOffset * pixelToMeterConstant;
            laserXOffset = laserXOffset * pixelToMeterConstant;
        }

        // converts degrees to radians because the
        // robot likes radians
        private void convertDegreesToRadians()
        {
            thetaOffset = (thetaOffset * Math.PI) / 180.0;
            Console.WriteLine("This is thetaOffset in radians: " + thetaOffset);
        }

        // gets camera image, finds offset from fiducial center,
        // instructs robot to move accordly

        //should be private
        public string getCameraString()
        {
            return cameraInterface.getCameraStringMsg();
        }
        
        public bool getCameraData()
        {
            return cameraInterface.getCameraData();
        }

        //Distances in meters
        public double getCameraTopDistance()
        {
             cameraInterface.getCameraMeasurements("LEFT");
             return cameraInfo.getTopDistance() * pixelToMeterConstant;
        }

        public double getCameraBottomDistance()
        {
            cameraInterface.getCameraMeasurements("RIGHT");
            return cameraInfo.getBottomDistance() * pixelToMeterConstant;
        }

        public double getCameraInBtwnDistance()
        {
            cameraInterface.getCameraMeasurements("LINE");
            return cameraInfo.getInBtwnDistance() * pixelToMeterConstant;
        }
        public bool getCameraMeasurementForCenter()
        {
            return cameraInterface.getCameraMeasurements("CENTER");
        }
        //public void moveRobotUp(double distance = 0.02)
        //{
        //    if (distance < .3)
        //        robot.getPoseOrMove("MOVE", 0.0, distance, 0.0);
        //}

        //public void moveRobotDown(double distance = 0.02)
        //{
        //    if (distance < .3)
        //        robot.getPoseOrMove("MOVE", 0.0, -distance, 0.0);
        //}

        //public void moveRobotLeft(double distance = 0.02)
        //{
        //    if (distance < .3)
        //        robot.getPoseOrMove("MOVE", distance, 0.0, 0.0);
        //}

        //public void moveRobotRight(double distance = 0.02)
        //{
        //    if (distance < .3)
        //        robot.getPoseOrMove("MOVE", -distance, 0.0, 0.0);
        //}

        //public void moveRobotForward(double distance = 0.02)
        //{
        //    if (distance < .3)
        //        robot.getPoseOrMove("MOVE", 0.0, 0.0, distance);
        //}

        //public void moveRobotBackward(double distance = 0.02)
        //{
        //    if (distance < .3)
        //        robot.getPoseOrMove("MOVE", 0.0, 0.0, -distance);
        //}


       

        public void goToOriginPose(double x, double y, double z, double Rx, double Ry, double Rz, double time)
        {
            robot.sendCommandPose(x, y, z, Rx, Ry, Rz, time);
        }

        public void goToOriginJointDeg(double Base, double Shoulder, double Elbow, double Wrist1, double Wrist2, double Wrist3, double time)
        {
            double ToRad = 3.1415 / 180;
            robot.sendCommandJoint(Base * ToRad, Shoulder * ToRad, Elbow * ToRad, Wrist1 * ToRad, Wrist2 * ToRad, Wrist3 * ToRad, time);
        }

        public void goToOriginJointRadians(double Base, double Shoulder, double Elbow, double Wrist1, double Wrist2, double Wrist3, double time)
        {
            robot.sendCommandJoint(Base, Shoulder, Elbow, Wrist1, Wrist2, Wrist3, time);
        }

        public void goToOriginJointRadians(string jointPositionsInRadians, double time)
        {
            robot.sendCommandJoint(jointPositionsInRadians, time);
        }

        public void centerFiducial(double toleranceInMeters = 0.00005)
        {
            try
            {
                // camera finds fiducial in frame and computes difference
                // from center frame
               
                calculateFiducialOffsetFromCenter();
                // converts to meters because robot likes meters
                convertPixelsToMeters();
                convertDegreesToRadians();
                if (Math.Abs(thetaOffset) > 0.5) thetaOffset = 0; //avoid 90 deg turns
                robot.getPoseOrMove("MOVE", yOffset, -xOffset, laserXOffset, thetaOffset);
                // keep centering until < 50 microns or tried more than 10 times
                // then move an offset and try to recenter
                int i = 0;
                double maxError = toleranceInMeters;
                while (true)
                {
                    if (Math.Abs(xOffset) > maxError || Math.Abs(yOffset) > maxError)
                    {
                        if (i < 5)
                        {
                            // Console.WriteLine("NOT CLOSE ENOUGH (less than than or equal 150 microns)");
                           calculateFiducialOffsetFromCenter();
                            convertPixelsToMeters();
                            convertDegreesToRadians();
                            robot.getPoseOrMove("MOVE", yOffset, -xOffset, laserXOffset, thetaOffset);
                            i++;
                        }
                        else
                        {
                            robot.getPoseOrMove("MOVE", 0.002, 0.0, 0.0);
                            calculateFiducialOffsetFromCenter();
                            convertPixelsToMeters();
                            convertDegreesToRadians();
                            robot.getPoseOrMove("MOVE", yOffset, -xOffset, laserXOffset, thetaOffset);
                            i = 0;
                        }
                    }
                    else { break; }
                    
                }
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }
        }


        //ROBOT ----------------------------------------------------------------------------------------------------------
        
        //for "Move" the angle is given in radians, '+' is CW looking from the chamber door to the robot
        //                                          '-' is CCW looking from the chamaber door to the robot
        public void moveFromCurrentPosition(double x, double y, double z)
        {
            robot.getPoseOrMove("MOVE", -x, y, z, 0);
        }

        public void moveToHpos()
        {
            robot.getPoseOrMove("MOVE", 0, 0, 0, -1.5708);
        }

        public void moveBack2Vpos()
        {
            robot.getPoseOrMove("MOVE", 0, 0, 0, 1.5708);
        }

        public void moveForThermalSnap()
        {
            moveFromCurrentPosition(0, 0.0825, 0);
        }

        public void moveBackFromThermalSnap()
        {
            moveFromCurrentPosition(0, -0.0825, 0);
        }


        //stores the current coordinates of the tcp, with respect to the variable called orgin
        public bool saveCoordinates(string fileName, bool header)
        {
            try
            {
                robot.getPoseOrMove("GETPOS");

                string[] temp = RobotInfo.toolPosString.Split(',');
                temp[0] = (double.Parse(temp[0]) * -1).ToString();
                for (int i = 0; i < temp.Length; i++)
                {
                    temp[i] = double.Parse(temp[i], System.Globalization.NumberStyles.Float,
                                                    System.Globalization.CultureInfo.InvariantCulture).ToString("N8");
                }

                string coord = string.Format("{0}, {1}, {2}, {3}, {4}, {5}", temp[0], temp[1],
                    temp[2], temp[3], temp[4], temp[5]);

                if (header)
                {
                    System.IO.File.AppendAllText(fileName, "x, y, z, Rx, Ry, Rz" + Environment.NewLine);
                }
                else {
                    System.IO.File.AppendAllText(fileName, coord + Environment.NewLine);
                }
                return true;
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
                return false;
                
            }
        }

        //store the current pos w.r.t. to the base (absolute) in joint coordinates
        public void saveJointPosition(string fileName)
        {
            robot.getPoseOrMove("GETPOS");
            System.IO.File.AppendAllText(fileName, RobotInfo.jointPosString + Environment.NewLine);
        }

        public void readFromFileJointPosition(string fileName)
        {
            jointPositions = System.IO.File.ReadAllLines(fileName);
            numberOfReadCoordinates = jointPositions.Length;
            Console.WriteLine("nbr of stored pos: "+ numberOfReadCoordinates.ToString());
            Log.WriteLine("Saved Current coordinates in " + fileName);
        }

        public void moveToJointPosition(int index, double time)
        {
            goToOriginJointRadians(jointPositions[index], time);
        }

        public void moveToLastSavedPosition(string filename)
        {
            readFromFileJointPosition(filename);                  //load coordinates
            moveToJointPosition(numberOfReadCoordinates - 1, 4);  //move back to previous
        }

        public void loadJointPosition()
        {
            robot.getPoseOrMove("GETPOS");
        }


        public void setOrigin()
        {
            robot.getPoseOrMove("SETORGIN");
        }

        public void ParkerSucksAug()
        {
            cameraInterface.getCameraData();
            
            
        }

        public double getX()
        {
            loadJointPosition();
            return RobotInfo.currentToolPosition[0];
        }

        public double getY()
        {
            loadJointPosition();
            return RobotInfo.currentToolPosition[1];
        }
        public double getZ()
        {
            loadJointPosition();
            return RobotInfo.currentToolPosition[2];
        }
        public double getRX()
        {
            loadJointPosition();
            return RobotInfo.currentToolPosition[3];
        }
        
        public double getRY()
        {
            loadJointPosition();
            return RobotInfo.currentToolPosition[4];
        }

        public double getRZ()
        {
            loadJointPosition();
            return RobotInfo.currentToolPosition[5];
        }

        public double getDistanceFromSensor()
        {
            return distanceSensor.getMeasurement();
        }

        //TR MODULE - Funcitions for the TR Module ---------------------------------------------------------------------------
        public byte[] getMFCregistry(double phaseValue, int attenValue, bool active)
        {
             byte[] MFC4bytes = new byte[4];
             System.UInt32 temp = mfcControlBit.GetTxData(phaseValue, attenValue, active);
            //System.UInt32 temp = mfc.;
            MFC4bytes = BitConverter.GetBytes( temp);
            //This build returns the 4 bytes reversed. So we nee
            //to reversed again
            Array.Reverse(MFC4bytes);
            return MFC4bytes;
        }
        public byte[] createConfigSendMsg(double[] phase, int[] atten, bool[] active)
        {
            return mfcControlBit.CreateConfigSendMsg(phase, atten, active);
        }

        //public void saveFlirImage(string fileName)
        //{
        //    // wantFrame = true;
        //    // flirInterface.updateFileName(fileName);
        //}

        //public void Close()
        //{

           // flirInterface.Stop();
          //  flirInterface.Disconnect();

        //}
        //private void startExperimentFromOrgin()
        //{
        //    double fidOffset = 0.0508;

        //    string imageWriteDir = baseExpirementDirectory + @"FlirImages\";         
        //    int counter = 0;

        //    System.IO.Directory.CreateDirectory(baseExpirementDirectory);
        //    System.IO.Directory.CreateDirectory(imageWriteDir);

        //    // flirInterface.Start(imageWriteDir);

        //    // Console.WriteLine("This is fid index length: " + fidIndexes.Count);



        //    robot.sendCommand(robotOrgins[0][0], robotOrgins[0][1], robotOrgins[0][2],
        //                       robotOrgins[0][3], robotOrgins[0][4], robotOrgins[0][5]);


        //   // fiducialBarcode = string.Format("({0},{1})", fidIndexes[counter][0], fidIndexes[counter][1]);

        //    centerFiducial();

          ///  robot.getPoseOrMove("SETORGIN");

        //    saveCoordsToolSpace(baseExpirementDirectory + resultFileName);

        //    moveRobotUp(0.0889);
        //    flirInterface.fidNumber = fiducialBarcode;
        //    wantFrame = true;
        //    moveRobotDown(0.0889);

        //    for (int x = 0; x < 8 && Running; x++)
        //    {
        //        for (int y = 0; y < 7 && Running; y++)
        //        {
        //            robot.getPoseOrMove("MOVE", 0, fidOffset);
        //            counter++;
        //            centerFiducial();
        //            fiducialBarcode = string.Format("({0},{1})", fidIndexes[counter][0], fidIndexes[counter][1]);
        //            robot.getPoseOrMove("GETPOS");
        //            saveCoordsToolSpace(baseExpirementDirectory + resultFileName);

        //            // move camera the offset of the Flir so that Flir can aquire image of the current fiducial
        //            moveRobotUp(0.0889);
        //            flirInterface.fidNumber = fiducialBarcode;
        //            wantFrame = true;
        //            moveRobotDown(0.0889);
        //        }

        //        if (counter < 63)
        //        {
        //            robot.getPoseOrMove("MOVE", 0, -(fidOffset * 7));
        //            robot.getPoseOrMove("MOVE", -fidOffset, 0);
        //            counter++;
        //            centerFiducial();
        //            fiducialBarcode = string.Format("({0},{1})", fidIndexes[counter][0], fidIndexes[counter][1]);
        //            robot.getPoseOrMove("GETPOS");
        //            saveCoordsToolSpace(baseExpirementDirectory + resultFileName);
        //        }
        //    }
        //}
        //public void stopRunning()
        //{
        //    Running = false;
        //}

        //public void runExperiment()
        //{
        //    // moves from my rough coordinate list, then centers at each fiducial and saves it's centered coords in mm to a file
        //    Running = true;
        //    Thread t = new Thread(startExperimentFromOrgin);
        //    t.Start();

        //    // computes the flatness by taking 3 indecies to make a plane and then calculates the distance from all the other fiducials
        //    // then write that information to a file in format fiducial#,x,y,z,distance in z
        //    // computeFlatnessWriteToFile(0, moveCoords.Count/2, (moveCoords.Count) - 1);
        //}

        //public void writeCoordsToFile(string fileName)
        //{
        //    // robot.getPoseOrMove(false);

        //    System.IO.File.AppendAllText(fileName, RobotInfo.jointPosString + Environment.NewLine);
        //}

        //public double calculateDistance(double[] equation, double x, double y, double z)
        //{
        //    double distance = ((equation[0] * x) + (equation[1] * y) + (equation[2] * z) + (equation[3])) /
        //        Math.Sqrt((Math.Pow(equation[0], 2) + Math.Pow(equation[1], 2) + Math.Pow(equation[2], 2)));

        //    distance *= 1000000;

        //    return distance;
        //}

        //public Vector3D convertPointsToVector(double a1, double a2, double a3, double b1, double b2, double b3)
        //{
        //    double x, y, z;
        //    x = (b1 - a1);
        //    y = (b2 - a2);
        //    z = (b3 - a3);
        //    Vector3D myVector = new Vector3D(x, y, z);
        //    return myVector;

        //}

        // pass 2 vectors and a random point on plane
        //public double[] makePlane(Vector3D A, Vector3D B, double x, double y, double z)
        //{
        //    //0 = x, 1=y, 2=z, 3=d
        //    double[] equation = new double[4];
        //    Vector3D crossProduct = new Vector3D();

        //    crossProduct = Vector3D.CrossProduct(A, B);

        //    equation[0] = crossProduct.X;
        //    equation[1] = crossProduct.Y;
        //    equation[2] = crossProduct.Z;
        //    equation[3] = equation[0] * -(x) + equation[1] * -(y) + equation[2] * -(z);

        //    //Console.WriteLine(String.Format("This is equation of plane: {0}x + {1}y + {2}z + {3} = 0", equation[0], equation[1],
        //    //    equation[2], equation[3]));
        //    return equation;
        //}
        //public void computeFlatnessWriteToFile(int p1Location, int p2Location, int p3Location, int panelIndex)
        //{
        //    getFiducialInfo();
        //    string outputFile = baseExpirementDirectory + outputFileName;
        //    double a1, a2, a3, b1, b2, b3, c1, c2, c3, d1, d2, d3, distance;
        //    Vector3D A = new Vector3D();
        //    Vector3D B = new Vector3D();
        //    double[] equation = new double[4];
        //    string barcode = "";
        //    a1 = moveCoords[p1Location][0];
        //    a2 = moveCoords[p1Location][1];
        //    a3 = moveCoords[p1Location][2];

        //    b1 = moveCoords[p2Location][0];
        //    b2 = moveCoords[p2Location][1];
        //    b3 = moveCoords[p2Location][2];

        //    c1 = moveCoords[p3Location][0];
        //    c2 = moveCoords[p3Location][1];
        //    c3 = moveCoords[p3Location][2];

        //    A = convertPointsToVector(a1, a2, a3, b1, b2, b3);
        //    B = convertPointsToVector(a1, a2, a3, c1, c2, c3);

        //    equation = makePlane(A, B, a1, a2, a3);

        //    System.IO.File.AppendAllText(outputFile, String.Format("## PANEL({0}) ##\nfiducialNumber,X (robot base view in meters),Y (robot base view in meters),Z (robot base view in meters), distance (micrometers)", panelIndex) + Environment.NewLine);
        //    for (int i = 0; i < moveCoords.Count; i++)
        //    {
        //        barcode = barcodes[i];
        //        d1 = moveCoords[i][0];
        //        d2 = moveCoords[i][1];
        //        d3 = moveCoords[i][2];

        //        distance = calculateDistance(equation, d1, d2, d3);

        //        System.IO.File.AppendAllText(outputFile, String.Format("{0},{1},{2},{3},{4}", barcode, d1, d2, d3, distance) + Environment.NewLine);
        //    }

        //public void readCoordsFromFile(string fileName)
        //{
        //    string text = System.IO.File.ReadAllText(fileName);
        //    string[] lines = System.IO.File.ReadAllLines(fileName);

        //    if (moveCoords.Count == 0)
        //    {
        //        foreach (string line in lines)
        //        {
        //            string[] temp = line.Split(',');
        //            double[] coords = new double[6];

        //            if (temp.Length != 6)
        //            {
        //                Console.WriteLine("Something is wrong with coordinate file (incorrect format)");
        //                break;
        //            }
        //            else
        //            {
        //                for (int i = 0; i < temp.Length; i++)
        //                {
        //                    coords[i] = Convert.ToDouble(temp[i]);
        //                }

        //            }
        //            moveCoords.Add(coords);
        //        }
        //    }
        //    else
        //    {
        //        moveCoords.Clear();
        //        foreach (string line in lines)
        //        {
        //            string[] temp = line.Split(',');
        //            double[] coords = new double[6];
        //            if (temp.Length != 6)
        //            {
        //                Console.WriteLine("Something is wrong with coordinate file (incorrect format)");
        //                break;
        //            }
        //            else
        //            {
        //                for (int i = 0; i < temp.Length; i++)
        //                {
        //                    coords[i] = Convert.ToDouble(temp[i]);
        //                }
        //            }
        //            moveCoords.Add(coords);
        //        }

        //    }
        //}

        //public void getFiducialInfo()
        //{
        //    if (moveCoords.Count == 0)
        //    {
        //        foreach (string line in toolHeadCoordinates)
        //        {
        //            string[] temp = line.Split(',');
        //            double[] coords = new double[6];
        //            string barcode = "";
        //            if (temp.Length != 7)
        //            {
        //                Console.WriteLine("Something is wrong with coordinate file (incorrect format)");
        //                break;
        //            }
        //            else
        //            {
        //                barcode = temp[0];
        //                barcodes.Add(barcode);

        //                for (int i = 1; i < temp.Length; i++)
        //                {
        //                    coords[i] = Convert.ToDouble(temp[i]);
        //                }

        //            }

        //            moveCoords.Add(coords);
        //        }
        //    }
        //    else
        //    {
        //        moveCoords.Clear();
        //        foreach (string line in toolHeadCoordinates)
        //        {
        //            string[] temp = line.Split(',');
        //            double[] coords = new double[6];
        //            string barcode = "";
        //            if (temp.Length != 7)
        //            {
        //                Console.WriteLine("Something is wrong with coordinate file (incorrect format)");
        //                break;
        //            }
        //            else
        //            {
        //                barcode = temp[0];
        //                barcodes.Add(barcode);
        //                for (int i = 1; i < temp.Length; i++)
        //                {
        //                    coords[i - 1] = Convert.ToDouble(temp[i]);
        //                }

        //            }
        //            moveCoords.Add(coords);
        //        }

        //    }
        //}

        //public void moveFromCoordListWriteToFile(int boardIndex, int index = -1)
        //{
        //    // string coordFile = robotCoordPath[boardIndex];
        //    string imageWriteDir = baseExpirementDirectory + @"FlirImages\";

        //    System.IO.Directory.CreateDirectory(baseExpirementDirectory);
        //    System.IO.Directory.CreateDirectory(imageWriteDir);
        //    flirInterface.Start(imageWriteDir);

        //    // readCoordsFromFile(coordFile);

        //    if (index == -1)
        //    {
        //        //for (int i = 0; i < (moveCoords.Count); i++)
        //        for (int i = 0; i < moveCoords.Count; i++)

        //        {
        //            // send move command to the robot in order of the file
        //            robot.sendCommand(moveCoords[i][0], moveCoords[i][1], moveCoords[i][2],
        //                              moveCoords[i][3], moveCoords[i][4], moveCoords[i][5]);
        //            // center fiducial once robot has arrived at position
        //            centerFiducial();
        //            // save the Coords in Toolspace so that we can use the data later
        //            fiducialBarcode = (i + 1).ToString();
        //            // saveCoordsToolSpace();

        //            // move camera the offset of the Flir so that Flir can aquire image of the current fiducial
        //            moveRobotUp(0.0889);
        //            // flirInterface.fidNumber = (i + 1);
        //            wantFrame = true;

        //        }

        //    }
        //    else
        //    {
        //        if (index > 0 && index < moveCoords.Count)
        //            robot.sendCommand(moveCoords[index - 1][0], moveCoords[index - 1][1], moveCoords[index - 1][2],
        //                                 moveCoords[index - 1][3], moveCoords[index - 1][4], moveCoords[index - 1][5]);

        //    }
        //    Thread.Sleep(400);
        //    flirInterface.Stop();
        //}
    }
}