﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.Windows;


namespace OU_System_Controller
{
    class RobotInterface
    {
        RobotInfo robotInfo;

        // constructor
        public RobotInterface()
        {

            robotInfo = new RobotInfo();
        }

        // Moves robot to a specific pose in base coordinates x,y,z,Rx,Ry,Rz
        public void sendCommandPose(double x, double y, double z, double aX, double aY, double aZ, double time)
        {
            try {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("192.168.0.55"),30002);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);


                sender.Connect(remoteEP);

                byte[] msg = Encoding.UTF8.GetBytes(String.Format("movel(p[{0}, {1}, {2}, {3}, {4}, {5}], t={6})\n", x, y, z, aX, aY, aZ, time));
                // Send the data through the socket.
                int bytesSent = sender.Send(msg);
                Console.WriteLine("This is command send to robot: " + Encoding.UTF8.GetString(msg));


                Thread.Sleep(4500);

                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            catch(Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }

        }


        // Moves robot to a specific join position base, shoulder, elbow, wrist1, wrist2, wrist3 RADIANS!!
        public void sendCommandJoint(string joints, double time)
        {
            try {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("192.168.0.55"), 30002);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);


                sender.Connect(remoteEP);

                byte[] msg = Encoding.UTF8.GetBytes(String.Format("movel([{0}], t={1})\n", joints, time));
                // Send the data through the socket.
                int bytesSent = sender.Send(msg);
                Console.WriteLine("This is command send to robot: " + Encoding.UTF8.GetString(msg));


                Thread.Sleep(4500);

                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }

        }

        //Coordinates are absolute 
        public void sendCommandJoint(double Base, double Shoulder, double Elbow, double Wrist1, double Wrist2, double Wrist3, double time)
        {
            try
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("192.168.0.55"), 30002);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);


                sender.Connect(remoteEP);

                byte[] msg = Encoding.UTF8.GetBytes(String.Format("movel([{0},{1},{2},{3},{4},{5}], t={6})\n", Base, Shoulder, Elbow, Wrist1, Wrist2, Wrist3, time));
                // Send the data through the socket.
                int bytesSent = sender.Send(msg);
                Console.WriteLine("This is command send to robot: " + Encoding.UTF8.GetString(msg));


                Thread.Sleep(4500);

                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }

        }
        // send command to robot to either move to x,y,z,theta or just receive current Pose of robot
        // commandFlag options (MOVE, GETPOS, SETORGIN)
        public void getPoseOrMove(string commandFlag, double x = 0.0, double y = 0.0, double z = 0.0, double theta = 0.0)
        {
                
            try
            {
                // robot connection point
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("192.168.0.55"), 29999);
                
                // create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    // connect to robot and send play command to begin program on robot
                    sender.Connect(remoteEP);
                    Console.WriteLine("connected to robot");
                    Thread.Sleep(100);
                    byte[] msg = Encoding.UTF8.GetBytes("play" + "\n");

                    // send the data through the socket.
                    int bytesSent = sender.Send(msg);
                    Console.WriteLine("sent1");
                    
                    // start listener program to listen for data sent from robot
                    TcpListener listener = new TcpListener(IPAddress.Any, 30000);
                    listener.Start();
                    Thread.Sleep(100);
                    // accept socket connection with robot
                    Socket client = listener.AcceptSocket();
                    Console.WriteLine("socket");
                    
                    byte[] data = new byte[100];
                    int size;
                    String line;

                    // i want to have the robot move
                    if (commandFlag == "MOVE")
                    {
                        msg = Encoding.UTF8.GetBytes(String.Format("({0},{1},{2},{3},2.0)\n", x, y, z, theta));
                        
                        bytesSent = client.Send(msg);
                        Console.WriteLine("sent");
                        data = new byte[100];
                        while (true)
                        {
                            size = client.Receive(data);
                            line = Encoding.UTF8.GetString(data, 0, size);

                            if (line == "DONE")
                            {
                                Console.WriteLine("Robot is finished");
                                break;
                            }


                        }
                    }
                    // i dont want robot to move, but want to get the current position back
                    else if (commandFlag == "GETPOS")
                    {
                        msg = Encoding.UTF8.GetBytes(String.Format("({0},{1},{2},{3},1.0)\n", 0.0, 0.0, 0.0, 0.0));
                        bytesSent = client.Send(msg);

                        data = new byte[200];

                        while (true)
                        {
                            size = client.Receive(data);
                            line = Encoding.UTF8.GetString(data, 0, size);
                            Console.WriteLine(line);
                            // robot code is done running
                            if (line == "DONE")
                            {
                                Console.WriteLine("Robot is finished");
                                break;
                            }

                            // tool head coordinates in meters from base view
                            else if (line == "tool")
                            {
                                size = client.Receive(data);
                                line = Encoding.UTF8.GetString(data, 0, size);

                                // send tool position to get parsed and stored
                                robotInfo.parseToolPosition(line);

                            }
                            else if (line == "joint")
                            {
                                size = client.Receive(data);
                                line = Encoding.UTF8.GetString(data, 0, size);

                                // send tool position to get parsed and stored
                                robotInfo.parseJointPosition(line);
                            }
                        }
                    }
                    else if (commandFlag == "SETORGIN")
                    {
                        msg = Encoding.UTF8.GetBytes(String.Format("({0},{1},{2},{3},3.0)\n", 0.0, 0.0, 0.0, 0.0));
                        bytesSent = client.Send(msg);

                        data = new byte[200];

                        while (true)
                        {
                            size = client.Receive(data);
                            line = Encoding.UTF8.GetString(data, 0, size);
                            
                            // robot code is done running
                            if (line == "DONE")
                            {
                                Console.WriteLine("Robot is finished");
                                line = "p[0,0,0,0,0,0]";
                                robotInfo.parseToolPosition(line);
                                break;
                            }

                            // tool head coordinates in meters from base view
                           
                                robotInfo.parseToolPosition(line);
                            
                        }
                    }

                    client.Close();
                    listener.Stop();

                    msg = Encoding.UTF8.GetBytes("stop" + "\n");
                    bytesSent = sender.Send(msg);

                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                    Thread.Sleep(100);

                }

                catch (Exception E)
                {
                    Log.WriteLine(E.Message + "\n" + E.StackTrace);
                }


            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }
        }
    }
}

