﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.Windows;


namespace OU_System_Controller
{
    class CameraInterface
    {

        CameraInfo cameraInfo;
        string CameraIPAddress = "192.168.0.10";
        // constructor
        public CameraInterface()
        {

            cameraInfo = new CameraInfo();
        }

        //polls camera for x,y,z,theta information
        public bool getCameraData()
        {
            try
            {


                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(CameraIPAddress), 49213);

                // Create a TCP/IP  socket.
                //Socket client = new Socket(AddressFamily.InterNetwork,
                //    SocketType.Stream, ProtocolType.Tcp);
                Socket client = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                client.ReceiveTimeout = 40000;
                client.Connect(remoteEP);

                // get data from socket
                byte[] data = new byte[100];
                int counter = 0;
                while (true)
                {
                    int size = client.Receive(data);
                    string line = Encoding.UTF8.GetString(data, 0, size);
                    Console.WriteLine("line from camera: " + line);

                    //send coords to get parsed and stored

                    if (cameraInfo.setFiducialCoords(line))
                        break;

                    if (counter > 20)
                    {

                        return false;
                    }
                    Thread.Sleep(150);
                    counter++;



                }
                client.Close();
                return true;
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
                return false;
            }
        }

        //This one is for the RF Measurement
        public bool getCameraMeasurements(string mode)
        {
            try
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(CameraIPAddress), 49213);

                // Create a TCP/IP  socket.
                Socket client = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                client.Connect(remoteEP);

                // get data from socket
                byte[] data = new byte[100];
                int counter = 0;
                while (true)
                {
                    int size = client.Receive(data);
                    string line = Encoding.UTF8.GetString(data, 0, size);
                    Console.WriteLine("line from camera: " + line);

                    //send coords to get parsed and stored
                    if (mode == "CENTER") // Fiducial coordinates
                        if (cameraInfo.setFiducialCoords(line))
                            break;

                    if (mode == "LEFT" || mode == "RIGHT" || mode == "LINE") // Distance between left border and center point
                        if (cameraInfo.setValuesFromCamString(line,mode))
                            break;



                    if (counter > 20)
                    {

                        return false;
                    }
                    Thread.Sleep(150);
                    counter++;



                }
                client.Close();
                return true;
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
                return false;
            }


        }

        //return the raw camera data
        //Function for Surface Characterization
        public string getCameraStringMsg()
        {
            try
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(CameraIPAddress), 49213);
                string line = "";
                // Create a TCP/IP  socket.
                Socket client = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                client.Connect(remoteEP);

                // get data from socket
                byte[] data = new byte[1000];
                int counter = 0;
                while (true)
                {
                    //receive message
                    int size = client.Receive(data);
                     line = Encoding.UTF8.GetString(data, 0, size);
                    
                    //print message
                    Console.WriteLine("line from camera: " + line);

                    if (line.Contains("CENTER")) break;


                }

                //close connection
                client.Close();
                return line;

                
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
                return "";
            }


        }
    }
}
