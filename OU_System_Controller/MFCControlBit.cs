﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OU_System_Controller
{
    public class MFCControlBit
    {
        private System.UInt32[] MfcPhaseDataBit, MfcAttenDataBit;
        private System.String[] strPhaseTemp, strAttenTemp;
       public MFCControlBit()
       {
           MfcPhaseDataBit = new System.UInt32[64];
           MfcAttenDataBit = new System.UInt32[64];
       }

       private UInt32 SETBIT(System.UInt32 address, int Bit)
       {
           UInt32 Reg;
           Reg = (address |= (UInt32)(1 << Bit));
           //Console.WriteLine(Convert.ToString(address,2) + " to " + Convert.ToString(Reg),2); 
           return Reg;
       }
       private UInt32 CLEARBIT(System.UInt32 address, int Bit)
       {
           UInt32 Reg;
           Reg = (address &= ~(UInt32)(1 << Bit));
           //Console.WriteLine(Convert.ToString(address, 2) + " to " + Convert.ToString(Reg), 2); 
           return Reg;
       }
       private bool TESTBIT(System.UInt32 address, int Bit)
        {
            bool Reg;
            Reg = (address & (UInt32)(1 << Bit)) == (UInt32)(1 << Bit);
            //Console.WriteLine(Convert.ToString(address, 2) + " to " + Convert.ToString(Reg), 2); 
           return Reg;
            
        }

        void Init()
{

	// Mfc Data Bit

    strPhaseTemp = new System.String[64];
    strAttenTemp = new System.String[64];

	strPhaseTemp[0] =  "0011001000000001000000000011"; 

	strPhaseTemp[1] =  "0011001000000001010000000010"; 

	strPhaseTemp[2] =  "0011001000000011001100000001"; 

	strPhaseTemp[3] =  "0011001000000011011100000000"; 

	strPhaseTemp[4] =  "0011001000000101000100000011"; 

	strPhaseTemp[5] =  "0011001000000101010100000010"; 

	strPhaseTemp[6] =  "0011001000000111001100000001"; 

	strPhaseTemp[7] =  "0011001000000111011100000010";

	strPhaseTemp[8] =  "0011001000001001001000000011"; 

	strPhaseTemp[9] =  "0011001000001001010100000010"; 

	strPhaseTemp[10] = "0011001000001001101100000010"; 

	strPhaseTemp[11] = "0011001000001001111100000000"; 

	strPhaseTemp[12] = "0011001000001101001000000011"; 

	strPhaseTemp[13] = "0011001000001101011000000011"; 

	strPhaseTemp[14] = "0011001000001111001100000001"; 

	strPhaseTemp[15] = "0011001000001111011100000010"; 

	strPhaseTemp[16] = "0011001000010001001000000010"; 

	strPhaseTemp[17] = "0011001000010001010100000010"; 

	strPhaseTemp[18] = "0011001000010001101100000010"; 

	strPhaseTemp[19] = "0011001000010011100100000010"; 

	strPhaseTemp[20] = "0011001000010101001100000011"; 

	strPhaseTemp[21] = "0011001000010101011100000010"; 

	strPhaseTemp[22] = "0011001000010111011000000001"; 

	strPhaseTemp[23] = "0011001000010111101100000001"; 

	strPhaseTemp[24] = "0011001000011001001100000010"; 

	strPhaseTemp[25] = "0011001000011001011100000010"; 

	strPhaseTemp[26] = "0011001000011001110100000000"; 

	strPhaseTemp[27] = "0011001000011011111000000000"; 

	strPhaseTemp[28] = "0011001000011101001100000001"; 

	strPhaseTemp[29] = "0011001000011101011100000001"; 

	strPhaseTemp[30] = "0011001000011111001100000010"; 

	strPhaseTemp[31] = "0011001000011111100100000010"; 

	strPhaseTemp[32] = "0011001000100001000000000011"; 

	strPhaseTemp[33] = "0011001000100001011000000001"; 

	strPhaseTemp[34] = "0011001000100011001100000010"; 

	strPhaseTemp[35] = "0011001000100011011100000010"; 

	strPhaseTemp[36] = "0011001000100101001100000001"; 

	strPhaseTemp[37] = "0011001000100101011100000010"; 

	strPhaseTemp[38] = "0011001000100111001100000010"; 

	strPhaseTemp[39] = "0011001000100111011100000010"; 

	strPhaseTemp[40] = "0011001000101001001100000001"; 

	strPhaseTemp[41] = "0011001000101001011100000001"; 

	strPhaseTemp[42] = "0011001000101011100000000000"; 

	strPhaseTemp[43] = "0011001000101011110000000000"; 

	strPhaseTemp[44] = "0011001000101101001100000001"; 

	strPhaseTemp[45] = "0011001000101101011100000001"; 

	strPhaseTemp[46] = "0011001000101111001100000010"; 

	strPhaseTemp[47] = "0011001000101111101000000010"; 

	strPhaseTemp[48] = "0011001000110001000100000001"; 

	strPhaseTemp[49] = "0011001000110001010100000001"; 

	strPhaseTemp[50] = "0011001000110011001100000010"; 

	strPhaseTemp[51] = "0011001000110011011100000010"; 

	strPhaseTemp[52] = "0011001000110101001100000001"; 

	strPhaseTemp[53] = "0011001000110101011100000011"; 

	strPhaseTemp[54] = "0011001000110111001100000011"; 

	strPhaseTemp[55] = "0011001000110111101000000001"; 

	strPhaseTemp[56] = "0011001000111001001100000001"; 

	strPhaseTemp[57] = "0011001000111001011100000001"; 

	strPhaseTemp[58] = "0011001000111011100000000001"; 

	strPhaseTemp[59] = "0011001000111011110000000000"; 

	strPhaseTemp[60] = "0011001000111101001000000001"; 

	strPhaseTemp[61] = "0011001000111101011000000001"; 

	strPhaseTemp[62] = "0011001000111111001100000001"; 

	strPhaseTemp[63] = "0011001000111111011100000001";



	strAttenTemp[0] =  "0011001000000001000000000011"; 

	strAttenTemp[1] =  "0011001000000001000000000111"; 

	strAttenTemp[2] =  "0011001000000001000000001011"; 

	strAttenTemp[3] =  "0011001000000001000000001111"; 

	strAttenTemp[4] =  "0011001000000001000000010011"; 

	strAttenTemp[5] =  "0011001000000001000000010111"; 

	strAttenTemp[6] =  "0011001000000001000000011011"; 

	strAttenTemp[7] =  "0011001000000001000000011111"; 

	strAttenTemp[8] =  "0011001000000001000010000011"; 

	strAttenTemp[9] =  "0011001000000001000010000111"; 

	strAttenTemp[10] = "0011001000000001000010001011"; 

	strAttenTemp[11] = "0011001000000001000010001111"; 

	strAttenTemp[12] = "0011001000000001000010010011"; 

	strAttenTemp[13] = "0011001000000001000010010111"; 

	strAttenTemp[14] = "0011001000000001000010011011"; 

	strAttenTemp[15] = "0011001000000000000010011111"; 

	strAttenTemp[16] = "0011001000000000000001100011"; 

	strAttenTemp[17] = "0011001000000000000001100111"; 

	strAttenTemp[18] = "0011001000000000000011001011"; 

	strAttenTemp[19] = "0011001000000000000011001111"; 

	strAttenTemp[20] = "0011001000000000000001110011"; 

	strAttenTemp[21] = "0011001000000000000001110111"; 

	strAttenTemp[22] = "0011001000000000000001111011"; 

	strAttenTemp[23] = "0011001000000000000011011111"; 

	strAttenTemp[24] = "0011001000000000000011100011"; 

	strAttenTemp[25] = "0011001000000000000011100111"; 

	strAttenTemp[26] = "0011001000000000000011101011"; 

	strAttenTemp[27] = "0011001000000000000011101111"; 

	strAttenTemp[28] = "0011001000000000000011110011"; 

	strAttenTemp[29] = "0011001000000000000011110111"; 

	strAttenTemp[30] = "0011001000000000000011111011"; 

	strAttenTemp[31] = "0011001000000000000011111111"; 



	for(int i=0;i<64;i++)

	{	

		MfcPhaseDataBit[i] = Convert.ToUInt32(strPhaseTemp[i],2);

	}



	for(int i=0;i<32;i++)

	{

        MfcAttenDataBit[i] = Convert.ToUInt32(strAttenTemp[i], 2);	

	}



}


        System.UInt32 GetTxCombineBit(System.UInt32 Data1, System.UInt32 Data2, bool active)
        {

            System.UInt32 lResult = 0;



            // Bit 0

            if ((TESTBIT(Data1, 0) && TESTBIT(Data2, 0)) )
           
                lResult=SETBIT(lResult, 0);
                  
            else

                lResult=CLEARBIT(lResult, 0);

            // Bit 1

            if ((TESTBIT(Data1, 1) && TESTBIT(Data2, 1)) )

                lResult=SETBIT(lResult, 1);

            else

                lResult=CLEARBIT(lResult, 1);

            // Bit 2

            if ((TESTBIT(Data1, 2) || TESTBIT(Data2, 2)) )

                lResult=SETBIT(lResult, 2);

            else

                lResult=CLEARBIT(lResult, 2);

            // Bit 3

            if ((TESTBIT(Data1, 3) || TESTBIT(Data2, 3)) )

                lResult=SETBIT(lResult, 3);

            else

                lResult=CLEARBIT(lResult, 3);

            // Bit 4

            if ((TESTBIT(Data1, 4) || TESTBIT(Data2, 4)) )

                lResult=SETBIT(lResult, 4);

            else

                lResult=CLEARBIT(lResult, 4);

            // Bit 5

            if (TESTBIT(Data1, 5) || TESTBIT(Data2, 5))

                lResult=SETBIT(lResult, 5);

            else

                lResult=CLEARBIT(lResult, 5);

            // Bit 6

            if (TESTBIT(Data1, 6) || TESTBIT(Data2, 6))

                lResult=SETBIT(lResult, 6);

            else

                lResult=CLEARBIT(lResult, 6);

            // Bit 7

            if (TESTBIT(Data1, 7) || TESTBIT(Data2, 7))

                lResult=SETBIT(lResult, 7);

            else

                lResult=CLEARBIT(lResult, 7);

            // Bit 8

            if (TESTBIT(Data1, 8) || TESTBIT(Data2, 8))

                lResult=SETBIT(lResult, 8);

            else

                lResult=CLEARBIT(lResult, 8);

            // Bit 9

            if (TESTBIT(Data1, 9) || TESTBIT(Data2, 9))

                lResult=SETBIT(lResult, 9);

            else

                lResult=CLEARBIT(lResult, 9);

            // Bit 10

            if (TESTBIT(Data1, 10) || TESTBIT(Data2, 10))

                lResult=SETBIT(lResult, 10);

            else

                lResult=CLEARBIT(lResult, 10);

            // Bit 11

            if (TESTBIT(Data1, 11) || TESTBIT(Data2, 11))

                lResult=SETBIT(lResult, 11);

            else

                lResult=CLEARBIT(lResult, 11);

            // Bit 12

            if (TESTBIT(Data1, 12) && TESTBIT(Data2, 12))

                lResult=SETBIT(lResult, 12);

            else

                lResult=CLEARBIT(lResult, 12);

            // Bit 13

            if (TESTBIT(Data1, 13) || TESTBIT(Data2, 13))

                lResult=SETBIT(lResult, 13);

            else

                lResult=CLEARBIT(lResult, 13);

            // Bit 14

            if (TESTBIT(Data1, 14) || TESTBIT(Data2, 14))

                lResult=SETBIT(lResult, 14);

            else

                lResult=CLEARBIT(lResult, 14);

            // Bit 15

            if (TESTBIT(Data1, 15) || TESTBIT(Data2, 15))

                lResult=SETBIT(lResult, 15);

            else

                lResult=CLEARBIT(lResult, 15);

            // Bit 16

            if (TESTBIT(Data1, 16) || TESTBIT(Data2, 16))

                lResult=SETBIT(lResult, 16);

            else

                lResult=CLEARBIT(lResult, 16);

            // Bit 17

            if (TESTBIT(Data1, 17) || TESTBIT(Data2, 17))

                lResult=SETBIT(lResult, 17);

            else

                lResult=CLEARBIT(lResult, 17);

            // Bit 18

            if (TESTBIT(Data1, 18) || TESTBIT(Data2, 18))

                lResult=SETBIT(lResult, 18);

            else

                lResult=CLEARBIT(lResult, 18);

            // Bit 19

            if (TESTBIT(Data1, 19) || TESTBIT(Data2, 19))

                lResult=SETBIT(lResult, 19);

            else

                lResult=CLEARBIT(lResult, 19);

            // Bit 20

            if (TESTBIT(Data1, 20) || TESTBIT(Data2, 20))

                lResult=SETBIT(lResult, 20);

            else

                lResult=CLEARBIT(lResult, 20);

            // Bit 21

            if (TESTBIT(Data1, 21) || TESTBIT(Data2, 21))

                lResult=SETBIT(lResult, 21);

            else

                lResult=CLEARBIT(lResult, 21);

            // Bit 22

            if (TESTBIT(Data1, 22) || TESTBIT(Data2, 22))

                lResult=SETBIT(lResult, 22);

            else

                lResult=CLEARBIT(lResult, 22);

            // Bit 23

            if (TESTBIT(Data1, 23) || TESTBIT(Data2, 23))

                lResult=SETBIT(lResult, 23);

            else

                lResult=CLEARBIT(lResult, 23);

            // Bit 24

            if (TESTBIT(Data1, 24) || TESTBIT(Data2, 24))

                lResult=SETBIT(lResult, 24);

            else

                lResult=CLEARBIT(lResult, 24);

            // Bit 25

            if (TESTBIT(Data1, 25) || TESTBIT(Data2, 25))

                lResult=SETBIT(lResult, 25);

            else

                lResult=CLEARBIT(lResult, 25);

            // Bit 26

            if (TESTBIT(Data1, 26) || TESTBIT(Data2, 26))

                lResult=SETBIT(lResult, 26);

            else

                lResult=CLEARBIT(lResult, 26);

            // Bit 27

            if (TESTBIT(Data1, 27) || TESTBIT(Data2, 27))

                lResult=SETBIT(lResult, 27);

            else

                lResult=CLEARBIT(lResult, 27);

            if (active) lResult = SETBIT(lResult, 27);
            else lResult = CLEARBIT(lResult, 27);

            return lResult;

        }




        
        public System.UInt32 GetTxData(double phaseValue, int attenValue, bool active)
        {

            System.UInt32 iResult;

            double p1 = 0;

            int a1 = 0;

            System.UInt32 TxPhaseValue, TxAttenValue;
            TxPhaseValue = 0;
            TxAttenValue = 0;
            /////////////////////////////////////////////////////////////////////////////// Phase 
            Init();
            p1 = phaseValue;

            if (p1 < 2.8125) { TxPhaseValue = MfcPhaseDataBit[0]; }

            else if (p1 >= 2.8125 && p1 < 8.4375) { TxPhaseValue = MfcPhaseDataBit[1]; }

            else if (p1 >= 8.4375 && p1 < 14.0625) { TxPhaseValue = MfcPhaseDataBit[2]; }

            else if (p1 >= 14.0625 && p1 < 19.6875) { TxPhaseValue = MfcPhaseDataBit[3]; }

            else if (p1 >= 19.6875 && p1 < 25.3125) { TxPhaseValue = MfcPhaseDataBit[4]; }

            else if (p1 >= 25.3125 && p1 < 30.9375) { TxPhaseValue = MfcPhaseDataBit[5]; }

            else if (p1 >= 30.9375 && p1 < 36.5625) { TxPhaseValue = MfcPhaseDataBit[6]; }

            else if (p1 >= 36.5625 && p1 < 42.1875) { TxPhaseValue = MfcPhaseDataBit[7]; }

            else if (p1 >= 42.1875 && p1 < 47.8125) { TxPhaseValue = MfcPhaseDataBit[8]; }

            else if (p1 >= 47.8125 && p1 < 53.4375) { TxPhaseValue = MfcPhaseDataBit[9]; }

            else if (p1 >= 53.4375 && p1 < 59.0625) { TxPhaseValue = MfcPhaseDataBit[10]; }

            else if (p1 >= 59.0625 && p1 < 64.6875) { TxPhaseValue = MfcPhaseDataBit[11]; }

            else if (p1 >= 64.6875 && p1 < 70.3125) { TxPhaseValue = MfcPhaseDataBit[12]; }

            else if (p1 >= 70.3125 && p1 < 75.9375) { TxPhaseValue = MfcPhaseDataBit[13]; }

            else if (p1 >= 75.9375 && p1 < 81.5625) { TxPhaseValue = MfcPhaseDataBit[14]; }

            else if (p1 >= 81.5625 && p1 < 87.1875) { TxPhaseValue = MfcPhaseDataBit[15]; }

            else if (p1 >= 87.1875 && p1 < 92.8125) { TxPhaseValue = MfcPhaseDataBit[16]; }

            else if (p1 >= 92.8125 && p1 < 98.4375) { TxPhaseValue = MfcPhaseDataBit[17]; }

            else if (p1 >= 98.4375 && p1 < 104.0625) { TxPhaseValue = MfcPhaseDataBit[18]; }

            else if (p1 >= 104.0625 && p1 < 109.6875) { TxPhaseValue = MfcPhaseDataBit[19]; }

            else if (p1 >= 109.6875 && p1 < 115.3125) { TxPhaseValue = MfcPhaseDataBit[20]; }

            else if (p1 >= 115.3125 && p1 < 120.9375) { TxPhaseValue = MfcPhaseDataBit[21]; }

            else if (p1 >= 120.9375 && p1 < 126.5625) { TxPhaseValue = MfcPhaseDataBit[22]; }

            else if (p1 >= 126.5625 && p1 < 132.1875) { TxPhaseValue = MfcPhaseDataBit[23]; }

            else if (p1 >= 132.1875 && p1 < 137.8125) { TxPhaseValue = MfcPhaseDataBit[24]; }

            else if (p1 >= 137.8125 && p1 < 143.4375) { TxPhaseValue = MfcPhaseDataBit[25]; }

            else if (p1 >= 143.4375 && p1 < 149.0625) { TxPhaseValue = MfcPhaseDataBit[26]; }

            else if (p1 >= 149.0625 && p1 < 154.6875) { TxPhaseValue = MfcPhaseDataBit[27]; }

            else if (p1 >= 154.6875 && p1 < 160.3125) { TxPhaseValue = MfcPhaseDataBit[28]; }

            else if (p1 >= 160.3125 && p1 < 165.9375) { TxPhaseValue = MfcPhaseDataBit[29]; }

            else if (p1 >= 165.9375 && p1 < 171.5625) { TxPhaseValue = MfcPhaseDataBit[30]; }

            else if (p1 >= 171.5625 && p1 < 177.1875) { TxPhaseValue = MfcPhaseDataBit[31]; }

            else if (p1 >= 177.1875 && p1 < 182.8125) { TxPhaseValue = MfcPhaseDataBit[32]; }

            else if (p1 >= 182.8125 && p1 < 188.4375) { TxPhaseValue = MfcPhaseDataBit[33]; }

            else if (p1 >= 188.4375 && p1 < 194.0625) { TxPhaseValue = MfcPhaseDataBit[34]; }

            else if (p1 >= 194.0625 && p1 < 199.6875) { TxPhaseValue = MfcPhaseDataBit[35]; }

            else if (p1 >= 199.6875 && p1 < 205.3125) { TxPhaseValue = MfcPhaseDataBit[36]; }

            else if (p1 >= 205.3125 && p1 < 210.9375) { TxPhaseValue = MfcPhaseDataBit[37]; }

            else if (p1 >= 210.9375 && p1 < 216.5625) { TxPhaseValue = MfcPhaseDataBit[38]; }

            else if (p1 >= 216.5625 && p1 < 222.1875) { TxPhaseValue = MfcPhaseDataBit[39]; }

            else if (p1 >= 222.1875 && p1 < 227.8125) { TxPhaseValue = MfcPhaseDataBit[40]; }

            else if (p1 >= 227.8125 && p1 < 233.4375) { TxPhaseValue = MfcPhaseDataBit[41]; }

            else if (p1 >= 233.4375 && p1 < 239.0625) { TxPhaseValue = MfcPhaseDataBit[42]; }

            else if (p1 >= 239.0625 && p1 < 244.6875) { TxPhaseValue = MfcPhaseDataBit[43]; }

            else if (p1 >= 244.6875 && p1 < 250.3125) { TxPhaseValue = MfcPhaseDataBit[44]; }

            else if (p1 >= 250.3125 && p1 < 255.9375) { TxPhaseValue = MfcPhaseDataBit[45]; }

            else if (p1 >= 255.9375 && p1 < 261.5625) { TxPhaseValue = MfcPhaseDataBit[46]; }

            else if (p1 >= 261.5625 && p1 < 267.1875) { TxPhaseValue = MfcPhaseDataBit[47]; }

            else if (p1 >= 267.1875 && p1 < 272.8125) { TxPhaseValue = MfcPhaseDataBit[48]; }

            else if (p1 >= 272.8125 && p1 < 278.4375) { TxPhaseValue = MfcPhaseDataBit[49]; }

            else if (p1 >= 278.4375 && p1 < 284.0625) { TxPhaseValue = MfcPhaseDataBit[50]; }

            else if (p1 >= 284.0625 && p1 < 289.6875) { TxPhaseValue = MfcPhaseDataBit[51]; }

            else if (p1 >= 289.6875 && p1 < 295.3125) { TxPhaseValue = MfcPhaseDataBit[52]; }

            else if (p1 >= 295.3125 && p1 < 300.9375) { TxPhaseValue = MfcPhaseDataBit[53]; }

            else if (p1 >= 300.9375 && p1 < 306.5625) { TxPhaseValue = MfcPhaseDataBit[54]; }

            else if (p1 >= 306.5625 && p1 < 312.1875) { TxPhaseValue = MfcPhaseDataBit[55]; }

            else if (p1 >= 312.1875 && p1 < 317.8125) { TxPhaseValue = MfcPhaseDataBit[56]; }

            else if (p1 >= 317.8125 && p1 < 323.4375) { TxPhaseValue = MfcPhaseDataBit[57]; }

            else if (p1 >= 323.4375 && p1 < 329.0625) { TxPhaseValue = MfcPhaseDataBit[58]; }

            else if (p1 >= 329.0625 && p1 < 334.6875) { TxPhaseValue = MfcPhaseDataBit[59]; }

            else if (p1 >= 334.6875 && p1 < 340.3125) { TxPhaseValue = MfcPhaseDataBit[60]; }

            else if (p1 >= 340.3125 && p1 < 345.9375) { TxPhaseValue = MfcPhaseDataBit[61]; }

            else if (p1 >= 345.9375 && p1 < 351.5625) { TxPhaseValue = MfcPhaseDataBit[62]; }

            else if (p1 >= 351.5625 && p1 < 357.1875) { TxPhaseValue = MfcPhaseDataBit[63]; }

            else if (p1 >= 357.1875) { TxPhaseValue = MfcPhaseDataBit[63]; }





            //////////////////////////////////////////////////////////////////////////////////   Atten

            a1 = (int)attenValue;

            if (a1 == 0) { TxAttenValue = MfcAttenDataBit[0]; }

            else if (a1 == 1) { TxAttenValue = MfcAttenDataBit[1]; }

            else if (a1 == 2) { TxAttenValue = MfcAttenDataBit[2]; }

            else if (a1 == 3) { TxAttenValue = MfcAttenDataBit[3]; }

            else if (a1 == 4) { TxAttenValue = MfcAttenDataBit[4]; }

            else if (a1 == 5) { TxAttenValue = MfcAttenDataBit[5]; }

            else if (a1 == 6) { TxAttenValue = MfcAttenDataBit[6]; }

            else if (a1 == 7) { TxAttenValue = MfcAttenDataBit[7]; }

            else if (a1 == 8) { TxAttenValue = MfcAttenDataBit[8]; }

            else if (a1 == 9) { TxAttenValue = MfcAttenDataBit[9]; }

            else if (a1 == 10) { TxAttenValue = MfcAttenDataBit[10]; }

            else if (a1 == 11) { TxAttenValue = MfcAttenDataBit[11]; }

            else if (a1 == 12) { TxAttenValue = MfcAttenDataBit[12]; }

            else if (a1 == 13) { TxAttenValue = MfcAttenDataBit[13]; }

            else if (a1 == 14) { TxAttenValue = MfcAttenDataBit[14]; }

            else if (a1 == 15) { TxAttenValue = MfcAttenDataBit[15]; }

            else if (a1 == 16) { TxAttenValue = MfcAttenDataBit[16]; }

            else if (a1 == 17) { TxAttenValue = MfcAttenDataBit[17]; }

            else if (a1 == 18) { TxAttenValue = MfcAttenDataBit[18]; }

            else if (a1 == 19) { TxAttenValue = MfcAttenDataBit[19]; }

            else if (a1 == 20) { TxAttenValue = MfcAttenDataBit[20]; }

            else if (a1 == 21) { TxAttenValue = MfcAttenDataBit[21]; }

            else if (a1 == 22) { TxAttenValue = MfcAttenDataBit[22]; }

            else if (a1 == 23) { TxAttenValue = MfcAttenDataBit[23]; }

            else if (a1 == 24) { TxAttenValue = MfcAttenDataBit[24]; }

            else if (a1 == 25) { TxAttenValue = MfcAttenDataBit[25]; }

            else if (a1 == 26) { TxAttenValue = MfcAttenDataBit[26]; }

            else if (a1 == 27) { TxAttenValue = MfcAttenDataBit[27]; }

            else if (a1 == 28) { TxAttenValue = MfcAttenDataBit[28]; }

            else if (a1 == 29) { TxAttenValue = MfcAttenDataBit[29]; }

            else if (a1 == 30) { TxAttenValue = MfcAttenDataBit[30]; }

            else if (a1 == 31) { TxAttenValue = MfcAttenDataBit[31]; }

            else if (a1 < 0) { TxAttenValue = MfcAttenDataBit[0]; }

            else if (a1 > 31) { TxAttenValue = MfcAttenDataBit[31]; }





            ///////////////////////////////////////////////////////////////////////////  

            iResult = GetTxCombineBit(TxPhaseValue, TxAttenValue, active);
            
            return (UInt32)iResult;// iResult;

        }


        //create a msg to configure the phase att and activate or terminate all elements. 
        //The output should be sent directly to the TR module
        public byte[] CreateConfigSendMsg(double[] phase, int[] atten, bool[] active)
        {
            int j = 0;
            byte[] MFC4bytes = new byte[4];
            byte[] resultReg = new byte[139];
            resultReg[0] = 0x16; //HEADER
            resultReg[1] = 0x16;
            resultReg[2] = 0x01; //DEST
            resultReg[3] = 0x02; //SOURCE
            resultReg[4] = 0x03; //CMD
            resultReg[5] = 0x00; //ID
            resultReg[6] = 0x00; //LENGTH 128 BYTES
            resultReg[7] = 0x80;

            resultReg[136] = 0x00; //CHECKSUM FIRST BYTE IS ALWAYS 0X00
            resultReg[138] = 0xF5; //TAIL

            //DATA FIELDS
            for (int i = 8; i < 136; i += 4)
            {
                //get MFC register for the current port
                //Console.WriteLine("J:" + j.ToString() + " i:"+ i.ToString());
                MFC4bytes = BitConverter.GetBytes(GetTxData(phase[j], atten[j], active[j]));

                //assign the config bytes to the command
                resultReg[i] = MFC4bytes[3];
                resultReg[i + 1] = MFC4bytes[2];
                resultReg[i + 2] = MFC4bytes[1];
                resultReg[i + 3] = MFC4bytes[0];
                //counter for next port
                j++;
            }

            //checksum
            resultReg[137] = calculateCheckSum(resultReg);
            //Console.WriteLine("MCF:"+ ByteArrayToHexString(resultReg));
            return resultReg;
        }

        public byte calculateCheckSum(byte[] dataBytes)
        {
             byte checkSum;
             checkSum = dataBytes[2];
             for (int i = 3; i < (dataBytes.Length-3); i++)
                 checkSum ^= dataBytes[i];
             return checkSum;
        }
    }
}
