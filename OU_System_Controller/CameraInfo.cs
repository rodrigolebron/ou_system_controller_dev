﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OU_System_Controller
{
    class CameraInfo
    {
        static private double fiducialX, fiducialY, fiducialTheta, laserLineX, leftDistance, rightDistance, inbtwnDistance;


        // constructor
        public CameraInfo()
        {

            fiducialX = -1.0;
            fiducialY = -1.0;
            fiducialTheta = -1.0;
            laserLineX = -1.0;

        }

        // stores fiducial and coord information makes sure all values are placed 
        // returns true if no errors in camera output, returns false if any error
        // was found
        public bool setFiducialCoords(string coords)
        {
            
            try
            {
                String[] temp;
                //coords = coords.Substring();
                    
                
                temp = coords.Split(',');
                 
                Log.WriteLine(temp[1]+","+ temp[2] + "," + temp[3]);

                //temp is the argument array
                if (temp[1] == "<error>" || temp[1] == "<error>")
                {
                    return false;
                }
                else
                {
                    fiducialX = Convert.ToDouble(temp[1]);
                    fiducialY = Convert.ToDouble(temp[2]);
                    Log.WriteLine("fiducialX:" + temp[1] + ",fiducialy:" + temp[2]);
                }
                if (temp[3] == "<error>")
                {
                    fiducialTheta = 90.0;
                    System.Console.WriteLine("Fiducial Data: "+fiducialTheta);
                    //Log.WriteLine("Fiducial Data" + fiducialTheta);
                }
                else if (Math.Abs(Convert.ToDouble(temp[3])*180/3.1415 - 90) > 20)
                {
                    return false;
                }
                else
                {
                    fiducialTheta = Convert.ToDouble(temp[3]) * 180 / 3.1415;
                }

                return true;

            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
                return false;
            }

        }


        public bool setValuesFromCamString(string coords, string mode)
        {

            try
            {
                String[] temp;
                int index = 0;
                

                temp = coords.Split(',');
                //Log.WriteLine(temp[0] + "," + temp[1] + "," + temp[2]);
                Console.WriteLine("Looking for strings");
                if (mode == "CENTER")
                {
                    //temp is the argument array
                    if (temp[1] == "<error>" || temp[2] == "<error>")
                    {
                        return false;
                    }
                    else
                    {
                        fiducialX = Convert.ToDouble(temp[0]);
                        fiducialY = Convert.ToDouble(temp[1]);
                    }
                    if (temp[3] == "<error>")
                    {
                        fiducialTheta = 90.0;
                        System.Console.WriteLine("Fiducial Data: " + fiducialTheta);
                        //Log.WriteLine("Fiducial Data" + fiducialTheta);
                    }
                    else if (Math.Abs(Convert.ToDouble(temp[3]) - 90) > 20)
                    {
                        return false;
                    }
                    else
                    {
                        fiducialTheta = Convert.ToDouble(temp[3]);
                    }
                }
                else if (mode == "LEFT")
                {
                    if (temp[index + 5] == "<error>")
                        return false;
                    else
                        leftDistance = Convert.ToDouble(temp[index + 5]);
                   
                }
                else if (mode == "RIGHT")
                {
                    if (temp[index + 7] == "<error>")
                        return false;
                    else
                        rightDistance = Convert.ToDouble(temp[index + 7]);

                    Console.WriteLine("Right Value: " + temp[index + 7]);
                }
                else if (mode == "LINE")
                {
                    if (temp[index + 9] == "<error>")
                        return false;
                    else
                        inbtwnDistance = Convert.ToDouble(temp[index + 9]);
                }
                
                return true;

            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
                return false;
            }

        }

        //returns array of coordinate values from camera
        public double[] getFiducialCoords()
        {
            double[] coords = new double[4];
            coords[0] = fiducialX;
            coords[1] = fiducialY;
            coords[2] = fiducialTheta;
            coords[3] = laserLineX;
            return coords;
        }
        //get distance from top border to center point
        public double getTopDistance()
        {
            return leftDistance;
        }

        //get distance from bottom border to center point
        public double getBottomDistance()
        {
            return rightDistance;
        }

        //get distance between bottom part and top of the next fiducial
        public double getInBtwnDistance()
        {
            return inbtwnDistance;
        }
    }
}
