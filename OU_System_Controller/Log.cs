﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;

namespace OU_System_Controller
{
    static class Log
    {
        private static LogLevels sLogLev = LogLevels.Info;

        private static String sName = "ErrorInfo";

        private static String sPath = "C:\\Logs";

        private static int iLogDuration = 30;

        private static Timer logChecker;


        public static void SetLevel(LogLevels logLevel)
        {
            sLogLev = logLevel;
        }

        public static void SetLevel(int logLevel)
        {
            sLogLev = (LogLevels)logLevel;

        }

        public static void SetName(String fileName)
        {
            sName = fileName;
        }

        public static void SetPath(String filePath)
        {
            sPath = filePath;
        }

        public static void SetDuration(int duration)
        {
            iLogDuration = duration;
        }

        public static void WriteLine(String s, LogLevels level)
        {
            if (level <= sLogLev)
            {
                try
                {
                    if (!Directory.Exists(sPath))
                    {
                        Directory.CreateDirectory(sPath);
                    }
                    string tPath = sPath + "\\" + sName + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
                    System.IO.File.AppendAllText(tPath, DateTime.Now.ToString() + " " + s + Environment.NewLine);

                }
                catch (Exception exce) { }
            }
            //}
        }

        public static void WriteLine(String s, int logLevel)
        {
            WriteLine(s, (LogLevels)logLevel);
        }

        public static void WriteLine(String s)
        {
            WriteLine(s, LogLevels.Warning);
        }

        public static void CleanAllLogs()
        {
            if (Directory.Exists(sPath))
            {
                string[] files = Directory.GetFiles(sPath);
                foreach (string file in files)
                {
                    //MessageBox.Show(file);
                    //ALL Logs
                    //if (file.Contains("ErrorInfo")) {
                    if (File.GetCreationTime(file) < DateTime.Now.AddDays(-iLogDuration))
                    {
                        File.Delete(file);
                    }
                    //}
                }
            }
        }

        public static void StartCleaner()
        {
            logChecker = new Timer();
            logChecker.Elapsed += new ElapsedEventHandler(logChecker_Elapsed);
            DateTime time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
            //Console.WriteLine(time.ToString());
            //time.AddDays(1);
            //Console.WriteLine(time.AddDays(1).ToString());
            logChecker.Interval = (time.AddDays(1) - DateTime.Now).TotalMilliseconds;
            logChecker.AutoReset = false;
            logChecker.Start();
        }

        static void logChecker_Elapsed(object sender, ElapsedEventArgs e)
        {
            CleanAllLogs();
            DateTime time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
            logChecker.Interval = (time.AddDays(1) - DateTime.Now).TotalMilliseconds;
            logChecker.Start();
        }


    }

    public enum LogLevels
    {
        Error = 0,
        Warning = 1,
        Info = 2,
        Debug = 3,
        Trace = 4
    };
}

