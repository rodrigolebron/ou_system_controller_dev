﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OU_System_Controller
{
    public class RobotInfo
    {
        static public double[] currentToolPosition;
        static private double[] currentJointPosition;
        static String robotCoords;
        static String[] temp;
        static public string jointPosString;
        static public string toolPosString;

        // constructor
        public RobotInfo()
        {
            
            currentToolPosition = new double[6];
            currentJointPosition = new double[6];
        }

        // parses and stores robots tool position in xyz space relative to the tool head
        // useful for moving linearly in tool space
        public void parseToolPosition(string pos)
        {
            try {
                Console.WriteLine("RobotCoords: " + pos);
                robotCoords = pos.ToString().Substring(0, pos.Length - 1).Substring(2);

                temp = robotCoords.Split(',');

                for (int i = 0; i < temp.Length; i++)
                {
                    currentToolPosition[i] = Convert.ToDouble(temp[i]);
                    //Console.WriteLine("This is currentToolPosition: " + currentToolPosition[i]);
                }
                toolPosString = robotCoords;
            }
            catch (Exception E)
            {
                Log.WriteLine(E.Message + "\n" + E.StackTrace);
            }
        }

        // parses and stores robots actual join positions of all 6 joints and stores them
        // useful for moving absolutely to a position
        public void parseJointPosition(string pos)
        {
            Console.WriteLine("RobotCoords: " + pos);
            robotCoords = pos.ToString().Substring(0, pos.Length - 1).Substring(1);
            jointPosString = robotCoords;
           
        }

        public double getToolPositionAt(int index)
        {
            return currentToolPosition[index];
        }

        public double getJointPositionAt(int index)
        {
            return currentJointPosition[index];
        }
         
    }
}
